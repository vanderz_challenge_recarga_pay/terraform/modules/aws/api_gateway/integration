variable "api_id" {}

variable "resource_id" {}

variable "http_method" {}

variable "cache_key_parameters" {}

variable "connection_id" {}

variable "connection_type" {}

variable "integration_http_method" {}

variable "type" {}

variable "content_handling" {}

variable "uri" {}

variable "request_parameters" {}

variable "passthrough_behavior" {}

variable "request_templates" {}

variable "integration_responses" {}