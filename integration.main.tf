resource "aws_api_gateway_integration" "main" {
  rest_api_id = var.api_id
  resource_id = var.resource_id
  http_method = var.http_method
  cache_key_parameters = var.cache_key_parameters
  connection_id = var.connection_id
  connection_type = var.connection_type
  content_handling = var.content_handling
  integration_http_method = var.integration_http_method
  type = var.type
  uri = var.uri
  request_parameters = var.request_parameters
  passthrough_behavior = var.passthrough_behavior
  request_templates = var.request_templates
}

resource "aws_api_gateway_integration_response" "main" {
  depends_on = [aws_api_gateway_integration.main]

  rest_api_id = var.api_id
  resource_id = var.resource_id
  http_method = var.http_method

  for_each = var.integration_responses
  status_code = each.key
  selection_pattern = each.value.selection_pattern
  response_templates = each.value.response_templates
  response_parameters = each.value.response_parameters

}